# osu-wineprefix

A tested 64-bits Wineprefix including `dotnet20`, `dotnet48` and `gdiplus_winxp`, assured to work for osu!

## Instructions

1. Use the `osu-winello` script: https://github.com/NelloKudo/osu-winello
2. Just download, extract it and use it as WINEPREFIX

## Steps to replicate (using Proton and umu)
```
WINEPREFIX=~/osu-umu PROTONPATH=~/.local/share/osuconfig/proton-osu/ GAMEID=osu-umu ~/.local/share/osuconfig/proton-osu/umu-run winetricks dotnet20 dotnet48 gdiplus_winxp meiryo win2k3
```

## Includes

- `dotnet20`
- `dotnet48`
- `gdiplus_winxp`
- `meiryo`

## Important stuff to not get me in trouble

#### This program is *in no way* related to Microsoft nor endorsed by Microsoft. `dotnet` and `gdiplus_winxp` are products of Microsoft.

## Winetricks logs

remove_mono internal
fontfix
dotnet20
remove_mono internal
remove_mono internal
winxp
dotnet40
dotnet48
gdiplus_winxp
meiryo
win2k3


